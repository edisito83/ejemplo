package delivery

import "github.com/Kong/go-pdk"

type EjemploConfig struct {
	EnableStatusOk bool `json:"enable_status_ok"`
}

func NewEjemploConfig() interface{} {
	return &EjemploConfig{}
}

func (conf *EjemploConfig) Access(kong *pdk.PDK) {
	kong.Log.Info("Hola Mundo", conf.EnableStatusOk)
}
