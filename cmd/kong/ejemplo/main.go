package main

import (
	delivery "gitlab.com/edisito83/ejemplo/internal/delivery/kong"

	"github.com/Kong/go-pdk/server"
)

func main() {
	server.StartServer(delivery.NewEjemploConfig, "0.1", 0)
}
